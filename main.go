package main

import (
	"blacktest/amqp"
	"blacktest/db"
	"encoding/json"
	"flag"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

const port = "8080"

func buildrouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", BuildHanlder(getUserList)).Methods("GET")
	router.HandleFunc("/", BuildHanlder(createUser)).Methods("POST")
	router.HandleFunc("/", BuildHanlder(optionsUser)).Methods("OPTIONS")
	return router
}

func saveUser(data []byte) {
	var userdata db.UserData
	if err := json.Unmarshal(data, &userdata); err != nil {
		log.Println(err)
	} else {
		user := db.User{Data: userdata}
		if err := db.GetDB().Insert(&user); err != nil {
			log.Println(err)
		} else {
			log.Printf("User with id %d is saved", user.Id)
		}
	}
}

func consumer() {
	isSave := false
	channel := amqp.Channel()
	msgs, err := channel.Consume(
		amqp.SAVE_QUEUE,
		"",    // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	if err != nil {
		log.Fatal(err)
	}
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
			if isSave {
				saveUser(d.Body)
			} else {
				amqp.PublishToForward(d.Body)
				log.Println("Message is forwarded")
				// Here it should notify a server (transmitter) about result of message handling.
			}
			isSave = !isSave
		}
	}()
	<-forever
}

func main() {
	mode := flag.String("mode", "", "possible value: migration, server, consumer")
	flag.Parse()
	switch *mode {
	case "migration":
		db.Migrate()
	case "server":
		log.Println("Start listening " + port)
		amqp.GetAmqpConnection()
		log.Fatal(http.ListenAndServe(":"+port, buildrouter()))
	case "consumer":
		consumer()
	default:
		log.Fatal("Pointed mode is not supported")
	}
}
