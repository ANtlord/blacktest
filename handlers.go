package main

import (
	"blacktest/amqp"
	"blacktest/db"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func getUserList(w http.ResponseWriter, r *http.Request) (HTTPResponse, HTTPError) {
	log.Println("getUserList")
	users := []db.User{}
	if err := db.GetDB().Model(&users).Select(); err != nil {
		return nil, NewInternalServerError(err)
	}
	return OKResponse{users}, nil
}

func createUser(w http.ResponseWriter, r *http.Request) (HTTPResponse, HTTPError) {
	log.Println("createUser")
	var userdata db.UserData
	// THere is should be validation before serialization into object.
	if decodeError := json.NewDecoder(r.Body).Decode(&userdata); decodeError != nil {
		return nil, NewBadRequest(decodeError)
	} else {
		if validationErrors := userdata.Validate(); len(validationErrors) > 0 {
			return nil, NewBadRequest(validationErrors)
		}
		if userdataDump, err := json.Marshal(&userdata); err != nil {
			return nil, NewInternalServerError(err)
		} else {
			if publishErr := amqp.PublishToSave(userdataDump); publishErr != nil {
				return nil, NewInternalServerError(publishErr)
			}
			return CreatedResponse{userdata}, nil
		}

	}
}

func optionsUser(w http.ResponseWriter, r *http.Request) (HTTPResponse, HTTPError) {
	return OKResponse{db.BuildUserDataOptions()}, nil
}

type endpoint func(w http.ResponseWriter, r *http.Request) (HTTPResponse, HTTPError)

// BuildHandler decorates handler making it appropriate for router
func BuildHanlder(ep endpoint) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		if response, httpErr := ep(w, r); httpErr != nil {
			w.Write([]byte(httpErr.Error()))
			w.WriteHeader(httpErr.StatusCode())
			log.Println(httpErr.StatusCode(), httpErr.Error())
		} else {
			status := response.StatusCode()
			if err := json.NewEncoder(w).Encode(response.GetData()); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				log.Println(err)
				return
			}
			w.WriteHeader(status)
			log.Println(fmt.Sprintf("%s return HTTP %d", r.URL, status))
		}
	})
}
