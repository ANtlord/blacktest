package amqp

import (
	"fmt"
	"log"
	"blacktest/conf"
	"github.com/streadway/amqp"
)

const (
	EXCHANGE_NAME = "users"
	SAVE_QUEUE = "for_save"
	FORWARD_QUEUE = "for_forward"
)

var amqpConnection *amqp.Connection
var amqpChannel *amqp.Channel

// GetAmqpConnection return AMQP connection.
func GetAmqpConnection() *amqp.Connection {
	if amqpConnection == nil {
		address := fmt.Sprintf("amqp://%s:%s@%s", conf.Amqp.Username, conf.Amqp.Password, conf.Amqp.Host)
		if conn, err := amqp.Dial(address); err != nil {
			log.Fatal(err)
		} else {
			amqpConnection = conn
		}
		log.Println("AMQP connection is establised")
		setup()
	}
	return amqpConnection
}

func buildQueue(name string) amqp.Queue {
	q, err := Channel().QueueDeclare(
		name,    // name
		true, // durable
		false, // delete when unused
		false,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Fatalf("Queue declaration '%s' is failed with %s", name, err)
	}
	log.Printf("Queue '%s' declared", name)
	return q
}

func setup() {
	channel := Channel()
	if err := channel.ExchangeDeclare(
		EXCHANGE_NAME,
		amqp.ExchangeDirect,
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	); err != nil {
		log.Fatal(err)
	}

	// Actually set names for queue is not good. It's better using exchanger and routing keys.
	buildQueue(SAVE_QUEUE)
	buildQueue(FORWARD_QUEUE)
}

// Channel return AMQP channel creating it if there is no channel.
func Channel() *amqp.Channel {
	if amqpChannel == nil {
		if channel, err := GetAmqpConnection().Channel(); err != nil {
			log.Fatal(err)
		} else {
			log.Println("AMQP channel is setup")
			amqpChannel = channel
		}
	}
	return amqpChannel
}

// PublishToSave sends message to queue for saving.
func PublishToSave(body []byte) error {
	return publish(body, SAVE_QUEUE)
}

// PublishToSave sends message to queue for forwarding.
func PublishToForward(body []byte) error {
	return publish(body, FORWARD_QUEUE)
}

func publish(body []byte, queue string) error {
	return Channel().Publish(
		"",
		queue,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         body,
		},
	)
}
