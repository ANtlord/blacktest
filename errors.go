package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// HTTPError is base for http error response
type HTTPError interface {
	Error() string
	StatusCode() int
}

// BadRequest for 400 http response
type BadRequest struct {
	data interface{}
}

func NewBadRequest(data interface{}) BadRequest {
	return BadRequest{data}
}

func (e BadRequest) Error() string {
	if data, err := json.Marshal(e.data); err != nil {
		panic(err)
	} else {
		return string(data[:])
	}
}

// StatusCode return status code for HTTP response
func (BadRequest) StatusCode() int {
	return http.StatusBadRequest
}

// InternalServerError for 500 http response
type InternalServerError struct {
	err error
}

func NewInternalServerError(err error) InternalServerError {
	return InternalServerError{err}
}

// StatusCode return status code for HTTP response
func (InternalServerError) StatusCode() int {
	return http.StatusInternalServerError
}

func (e InternalServerError) Error() string {
	log.Panicln(e.err)
	return "BadRequest"
}
