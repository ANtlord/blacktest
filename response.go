package main

import "net/http"

// HTTPResponse represens success finished repsponses
type HTTPResponse interface {
	StatusCode() int
	GetData() interface{}
}

type OKResponse struct {
	Data interface{}
}

func (OKResponse) StatusCode() int {
	return http.StatusOK
}

func (r OKResponse) GetData() interface{} {
	return r.Data
}

type CreatedResponse struct {
	Data interface{}
}

func (CreatedResponse) StatusCode() int {
	return http.StatusCreated
}

func (r CreatedResponse) GetData() interface{} {
	return r.Data
}
