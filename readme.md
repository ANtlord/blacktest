# Blacktest

Test assignment for blackmoon

## Dependencies
Postgres (tested on 9.6)
RabbitMQ (tested on 3.6.10)
Go (tested 1.10.3)

## Runnning
* Copy .env.dist to .env. The  file contains settings
* Edit .env consider your application environment
* Run `export $(cat ./.env| xargs)`
* Create database consider the settings
* Run `go build` in project directory
* Run `./blacktest -migrate` in order to create table in database.
* Run `./blacktest -server` in order to start HTTP server.
* Run `./blacktest -consumer` in order to start RabbitMQ worker.

## Testing

* There is few tests. Run them `go test ./...`
* Copy data.json.dist data.json. The file contains valid data. You can change it to test the validation.
* In order to create a user run `curl -H "Content-Type: application/json" -X POST -d @data.json http://127.0.0.1:8080/` 
* In order to get created users `curl -H "Content-Type: application/json" -X GET http://127.0.0.1:8080/` 
* In order to get endpoint options `curl -H "Content-Type: application/json" -X OPTIONS http://127.0.0.1:8080/` 
* Check messages running `rabbitmqctl list_queues name messages_ready messages_unacknowledged`
Queue `for_forward` contains messages that are forwarded.
