package db

import (
	"fmt"
)

// Migrate creates required tables
func Migrate() {
	fmt.Printf("Migration is started\n")
	db := GetDB()
	_, err := db.Exec(`
		CREATE TABLE users(
			id serial PRIMARY KEY,
			created_at timestamp with time zone not null DEFAULT now(),
			data jsonb not null
		);
	`)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Migration is finished\n")
}
