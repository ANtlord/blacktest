package db

import (
	"testing"
	"reflect"
)

func updateUserData(fieldName string, value interface{}, userdata *UserData) {
	reflectedUserData := reflect.ValueOf(userdata)
	elemUserData := reflectedUserData.Elem()
	field := elemUserData.FieldByName(fieldName)
	field.Set(reflect.ValueOf(value))
}

func newUserData() UserData {
	return UserData{
		FirstName: "Joe",
		LastName: "Black",
		Phone: "202-333-5005",
		Age: 21,
		Email: "jblack@fake.fk",
		MusicGenres: []string{"Jazz", "Blues"},
		Status: "Music in my soul",
		HasAnotherAccount: true,
	}
}

func TestUserDataValidate(t *testing.T) {
	userdata := newUserData()
	errors := userdata.Validate()
	if len(errors) > 0 {
		t.Errorf("Errors must be 0, actual %d", len(errors))
	}
}

func TestUserDataValidateFirstName(t *testing.T) {
	valuePairs := []struct{
		errorFieldName	string
		invalidValue	interface{}
	}{
		{"FirstName", ""},
		{"LastName", ""},
		{"Phone", ""},
		{"Age", 11},
		{"Email", ""},
		{"Email", "notvalidemail"},
		{"MusicGenres", []string{}},
		{"MusicGenres", []string{"NotAllowedGenre"}},
		{"Status", ""},
	}
	for _, pair := range valuePairs {
		userdata := newUserData()
		updateUserData(pair.errorFieldName, pair.invalidValue, &userdata)
		errors := userdata.Validate()
		if len(errors) != 1 {
			t.Errorf("%s", errors)
		} else if _, ok := errors[pair.errorFieldName]; !ok {
			t.Errorf("%s", errors)
		}
	}
}

func TestcheckString(t *testing.T) {
	possibleValues := []string{"Hello", "Goodbye"}
	valuePairs := []struct {
		word			string
		expectedResult	bool
	}{
		{"Hello", true},
		{"Good day", false},
	}

	for _, pair := range valuePairs {
		if checkString(&pair.word, possibleValues) != pair.expectedResult {
			t.Errorf("word: %s, possibleValues: %s", pair.word, possibleValues)
		}
	}
}
