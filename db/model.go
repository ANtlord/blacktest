package db

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"
)

const (
	REQUIRED_FIELD = "This field is required"
	INVALID_VALUE  = "Invalid value"
	MIN_AGE        = 12
	GREATER_VALUE  = "The value must be greater"
	ANY_VALUE      = "any"
	INVALID_EMAIL  = "Invalid e-mail address"
	EMAIL_REGEX    = `^([\w\.\_]{2,10})@(\w{1,}).([a-z]{2,4})$`
)

// User stores user data.
type User struct {
	Id        int
	CreatedAt time.Time `sql:"default:now()"`
	Data      UserData
}

// UserData represents data
type UserData struct {
	FirstName         string
	LastName          string
	Phone             string
	Age               int
	Email             string
	MusicGenres       []string
	Status            string
	HasAnotherAccount bool
}

func possibleMusicGenres() []string {
	return []string{"Folk", "Blues", "Jazz", "Disco", "Country"}
}

func stringFieldDescription() fieldDescription {
	return fieldDescription{"text", ANY_VALUE, true}
}

type fieldDescription struct {
	Type           string
	PossibleValues interface{}
	Required       bool
}

type UserDataOptions map[string]fieldDescription

// BuildUserDataOptions returns data appropriate for OPTIONS request.
func BuildUserDataOptions() UserDataOptions {
	data := make(map[string]fieldDescription)
	data["FirstName"] = stringFieldDescription()
	data["LastName"] = stringFieldDescription()
	data["Phone"] = stringFieldDescription()
	data["Age"] = fieldDescription{"integer", ANY_VALUE, true}
	data["Email"] = stringFieldDescription()
	data["MusicGenres"] = fieldDescription{"checkbox", possibleMusicGenres(), true}
	data["Status"] = fieldDescription{"textarea", ANY_VALUE, true}
	data["HasAnotherAccount"] = fieldDescription{"radio", []bool{true, false}, true}
	return data
}

func checkString(value *string, inValues []string) bool {
	res := false
	for _, item := range inValues {
		if item == *value {
			res = true
			break
		}
	}
	return res
}

func (o UserDataOptions) hasPositiveLen(sizeable interface{}, fieldName string, errors map[string]string) {
	if o[fieldName].Required && reflect.ValueOf(sizeable).Len() == 0 {
		errors[fieldName] = REQUIRED_FIELD
	}
}

// Validate check user data.
func (ud *UserData) Validate() map[string]string {
	options := BuildUserDataOptions()
	errors := make(map[string]string)

	options.hasPositiveLen(ud.FirstName, "FirstName", errors)
	options.hasPositiveLen(ud.LastName, "LastName", errors)
	options.hasPositiveLen(ud.Phone, "Phone", errors)
	options.hasPositiveLen(ud.Email, "Email", errors)
	options.hasPositiveLen(ud.MusicGenres, "MusicGenres", errors)
	options.hasPositiveLen(ud.Status, "Status", errors)

	possibleGenres := options["MusicGenres"].PossibleValues.([]string)
	for _, genre := range ud.MusicGenres {
		if !checkString(&genre, possibleGenres) {
			errors["MusicGenres"] = fmt.Sprintf(
				"%s. Use %s", INVALID_VALUE,
				strings.Join(possibleMusicGenres(), ", "),
			)
		}
	}
	if ud.Age < MIN_AGE {
		errors["Age"] = fmt.Sprintf("%s than %d", GREATER_VALUE, MIN_AGE)
	}
	if m, _ := regexp.MatchString(EMAIL_REGEX, ud.Email); !m {
		errors["Email"] = INVALID_EMAIL
	}
	// There is should be phone validation
	return errors
}
