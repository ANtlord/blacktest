package db

import (
	"log"
	"blacktest/conf"

	"github.com/go-pg/pg"
)

var db *pg.DB

func init() {
	if db == nil {
		db = pg.Connect(&pg.Options{
			User:     conf.Db.Username,
			Addr:     conf.Db.Host,
			Password: conf.Db.Password,
			Database: conf.Db.Name,
		})
		var n int
		if _, err := db.QueryOne(pg.Scan(&n), "SELECT 1"); err != nil {
			log.Fatal(err)
		}
		log.Println("Database connection is established")
	}
}

// GetDB Returns connection.
func GetDB() *pg.DB {
	if db == nil {
		log.Fatal("Database connection is not established")
	}
	return db
}
