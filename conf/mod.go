package conf

import "os"

func getEnv(name string) string {
	if val, ok := os.LookupEnv(name); !ok {
		panic("Environment variable " + name + " is not set.")
	} else {
		return val
	}
}

func init() {
	Db.Username = getEnv("BLACKTEST_DB_USERNAME")
	Db.Password = getEnv("BLACKTEST_DB_PASSWORD")
	Db.Host = getEnv("BLACKTEST_DB_HOST")
	Db.Name = getEnv("BLACKTEST_DB_NAME")

	Amqp.Username = getEnv("BLACKTEST_AMQP_USERNAME")
	Amqp.Password = getEnv("BLACKTEST_AMQP_PASSWORD")
	Amqp.Host = getEnv("BLACKTEST_AMQP_HOST")
}

// Db is database settings
var Db struct {
	Username string
	Password string
	Host string
	Name string
}

var Amqp struct {
	Username string
	Password string
	Host string
}
